package io.muic.ooc.lectures5.unittestdemo;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by panteparak on 1/24/17.
 */
public class UserServiceTest {
    @Test
    public void testEmptyPassword() throws Exception {
        UserService userService = new UserService();
        assertFalse("Empty Strings should be false", userService.checkIfPasswordValid(""));
    }

    @Test
    public void testNullPassword() throws Exception {
        UserService userService = new UserService();
        assertFalse("null should be false", userService.checkIfPasswordValid(null));

    }
}