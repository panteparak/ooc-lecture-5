package io.muic.ooc.lectures5.unittestdemo;

/**
 * Created by panteparak on 1/24/17.
 */
public class UserService {

    /**
     * This method is used to check whether the pattern of the password
     * is correct or not!
     * @param password string of password
     * @return true if it is not empty, blank or null and it must be at least 8 characters long,
     * must not begin and end with white space
     */
    public boolean checkIfPasswordValid(String password){
        if (password == null) return false;
        if (password.isEmpty()) return false;
        return true;

    }
}
